const mongoose = require("mongoose");

let isConnected = false;
export default async function connectToMongoose() {
  try {
    if (isConnected) {
      return;
    }
    mongoose.connect("mongodb+srv://andy:123@kenziefunreader.owxma.mongodb.net/glgo?authSource=admin", {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });
    isConnected = true;
  } catch (err) {
    console.log(err);
    throw err;
  }
}
