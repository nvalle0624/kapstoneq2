import cors from "cors";
import connectToMongoose from "./db/mongoose";
require("./db/mongoose");
const mongoose = require("mongoose");
const express = require("express");
const app = express();
const Users = require("./models/users");

mongoose.Promise = global.Promise;

app.use(cors());
app.use(express.static("./src"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(async (req, res, next) => {
  try {
    await connectToMongoose();
    next();
  } catch (err) {
    console.log(err);
    next(err);
  }
});

const userObj = [];

app.post("/api/userProfile", async (req, res) => {
  const userProfile = await Users.find({ email: req.body.email });
  userObj.push(userProfile);
  console.log(userProfile);
  res.status(200);
  res.send(userProfile);
});

app.post("/api/game", (req, res) => {
  Users.create(req.body);
  console.log(req.body);
});

export default app;
