const mongoose = require("mongoose");

mongoose.Promise = global.Promise;

// const objId = Schema.ObjectId;

const useSchema = new mongoose.Schema({
  firstname: String,
  lastname: String,
  email: String,
  password: String,
  age: Number,
  profilePic: String,
  active: Boolean,
  education: Array,
  interests: Array,
});

const Users = mongoose.model("Users", useSchema);

module.exports = Users;
