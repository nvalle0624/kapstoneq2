import { getConfig } from "@testing-library/react";
import { getFips } from "crypto";
import multer from "multer";
import path from "path";
import { config } from "process";
import { v4 } from "uuid";
const GridFsStorage = require(`multer-gridfs-storage`);

const uploadDirectory = "/uploads";

const diskStorage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, uploadDirectory);
  },
  filename: (req, file, callback) => {
    const fileExtension = path.extname(file.originalname);
    const filename = `${v4()}${fileExtension}`;
    callback(null, filename);
  },
});
// storage engine
const storage = new GridFsStorage({
  url: config.mongoURI,
  file: (req, file) => {
  return new Promise((resolve, reject) => {
    //encrypt filename first before storing
    crypto.randomBytes(16, (err, buf) => {
      if(err) {
        return reject (err);
      }const filename = buf.toString('hex') + path.extname(file.originalname);
        const fileInfo = {
          filename: filename,
          bucketName: 'uploads'
        };
        resolve(fileInfo);
    });
  });
  }
});
//initialize gridfs stream
const url = config.mongoURI;
const connect = mongoose.createConnection(url, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
    
    let gfs;

    connect.once('open', () => {
      gfs = new mongoose.mongo.GridFsBucket(connect.db, {
        bucketName: "uploads"
      });
    });
  //Upload single image
  imageRouter.route("/")
  .post(upload.single('file'), (req, res, next) => {
    console.log(req.body);
    Image.findOne({caption: req.body.caption})
    .then((image) => { 
      console.log(image);
    if(image) {
      return res.status(200).json({
        success: false,
        message: 'Image already exist',
      });
    }
    let newImage = new Image ({
      caption: req.body.caption,
      filename: req.file.filename,
      filed: req.file.id,
    });

    newImage.save()
    .then((image) => {

      res.status(200).json({
        success: true,
        image,
      });
    })
    .catch(err => res.status(500).json(err));
    })
   .catch(err => res.status(500).json(err));

app.post("/", upload.single(photo), (req, res) => {
  res.send("File upload success!")
})
//Get: fetches particular image and render to browser
ImageRouter.route('/image/:filename')
.get(( req, res, next) => {
  gfs.find({filename: req.params.filename }).toArray((err, files ) =>{
    if(!files[0] || files.length === 0) {
      return res.status(200).json ({
        success: false,
        message: 'No files available',
      });
    }
    if(files[0].contentType === 'image/jpeg'
    || files[0].contentType === 'image/png'
    || files[0].contentType === 'image/svg+xml') {
      //render image to browser
      gfs.openDownloadStreamByName(req.params.filename).pipe(res);
    } else {
      res.ststus(404).json({
          err:'Not an image',
      });
    }
  });
});


export const diskUploader = multer ({ storage: diskStorage})
