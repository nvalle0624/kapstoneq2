import "./App.css";
import Navigation from "./components/Navigation";
import Links from "./components/Links";

function App(props) {
  return (
    <div className="App">
      <Links />
      <Navigation />
    </div>
  );
}

export default App;
