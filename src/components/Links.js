import { NavLink } from "react-router-dom";
import { Nav } from "react-bootstrap";
import "./Links.css";
import useAuth from "../hooks/useAuth";

export default function Links() {
  const auth = useAuth();
  return (
    <>
      <Nav>
        <NavLink to="/Login" className="loginLink">
          {auth.loggedIn ? "Log Out" : "Log In"}
        </NavLink>
        <NavLink to="/Sign-Up" className="signupLink">
          Sign Up
        </NavLink>
      </Nav>
      <Nav>
        <NavLink to="/" className="homeLink">
          Home
        </NavLink>

        <NavLink to="/Dashboard" className="dashlink">
          Dashboard
        </NavLink>

        <NavLink to="/Settings" className="setlink">
          Settings
        </NavLink>
      </Nav>
    </>
  );
}
