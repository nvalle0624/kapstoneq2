import React from "react";
import { Switch } from "react-router-dom";
import ConnectedRoute from "./ConnectedRoute";
import Home from "../screens/Logo/Logo";
import NotFound from "../screens/NotFound";
import Dashboard from "../screens/Dashboard/Dashboard";
import SignUp from "./../screens/SignUp/SignUp";
import Login from "./../screens/Login/Login";
import Settings from "../screens/Settings/Settings";
import LogOut from "../screens/LogOut/LogOut";

export default function Navigation() {
  return (
    <Switch>
      <ConnectedRoute path="/Login" redirectIfAuthenticated component={Login} />
      <ConnectedRoute exact path="/" component={Home} />
      <ConnectedRoute path="/Sign-Up" component={SignUp} />
      <ConnectedRoute isProtected path="/LogOut" component={LogOut} />
      <ConnectedRoute isProtected path="/Dashboard" component={Dashboard} />
      <ConnectedRoute isProtected path="/Settings" component={Settings} />
      <ConnectedRoute path="*" component={NotFound} />
    </Switch>
  );
}
