import useMagicLink from "use-magic-link";

export default function useAuth() {
  return useMagicLink("pk_test_017DC4DB72651F1D");
}
