export const initialState = {
  topic: "",
  word: "",
  partOfSpeech: "",
  definition: "",
  audioClip: "",
  wordCollection: [],
};

export default initialState;
