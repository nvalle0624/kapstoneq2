import DefinitionComponent from "../../screens/Game/DefinitionComponent";

import { connect } from "react-redux";

const mapStateToProps = (state) => ({
  ...state,
  definition: state.definitionReducer.definition,
  pos: state.posReducer.pos,
});

export default connect(mapStateToProps)(DefinitionComponent);
