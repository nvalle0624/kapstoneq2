import TopicComponent from "../../screens/Game/TopicComponent";
import { connect } from "react-redux";

const mapStateToProps = (state) => ({
  topic: state.topicReducer.topic,
});

export default connect(mapStateToProps)(TopicComponent);
