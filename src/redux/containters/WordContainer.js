import WordComponent from "../../screens/Game/WordComponent";
import { connect } from "react-redux";

const mapStateToProps = (state) => ({
  ...state,
  word: state.wordReducer.word,
});

export default connect(mapStateToProps)(WordComponent);
