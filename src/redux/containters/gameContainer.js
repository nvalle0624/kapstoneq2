import Game from "../../screens/Game/Dictionary-Game";
import { connect } from "react-redux";

const mapStateToProps = (...state) => ({
  topic: state.topic,
  word: state.word,
  definition: state.definition,
  audioClip: state.audioClip,
  partOfSpeech: state.partOfSpeech,
  wordCollection: state.wordCollection,
});

export default connect(mapStateToProps(Game));
