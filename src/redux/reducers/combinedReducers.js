import { combineReducers } from "redux";
import topicReducer from "./topicReducer";
import wordReducer from "./wordReducer";
import definitionReducer from "./definitionReducer";
import posReducer from "./posReducer";

export default combineReducers({
  topicReducer,
  posReducer,
  wordReducer,
  definitionReducer,
});
