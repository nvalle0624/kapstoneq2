function definitionReducer(state = {}, action) {
  console.log("defintion reducer ran");
  switch (action.type) {
    case "CHANGE_DEFINITION":
      return { definition: action.definition };
    default:
      return state;
  }
}

export default definitionReducer;
