function posReducer(state = {}, action) {
  console.log(action);
  switch (action.type) {
    case "GET_POS":
      const pos = action.pos;
      console.log(pos);
      return { pos };
    default:
      return state;
  }
}

export default posReducer;
