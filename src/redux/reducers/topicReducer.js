const topics = ["sports", "music", "food", "science", "math", "dogs", "fish", "space", "technology"];

export const newTopic = [];
export const selectRandomTopic = () => {
  const randomTopicArrayIndex = Math.floor(Math.random() * topics.length);
  newTopic.push(topics[randomTopicArrayIndex]);
  return topics[randomTopicArrayIndex];
};
export const randomTopic = selectRandomTopic();

function topicReducer(state = {}, action) {
  console.log("topic reducer ran");
  switch (action.type) {
    case "CHANGE_TOPIC":
      return { topic: selectRandomTopic() };
    default:
      return state;
  }
}

export default topicReducer;
