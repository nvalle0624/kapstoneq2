import initialState from "../State";
import { store } from "../../index";

// import { randomTopic } from "../reducers/topicReducer";

// const topics = ["sports", "music", "food", "science", "math", "dogs", "fish", "space", "technology"];

// const newTopic = [];

// const selectRandomTopic = () => {
//   const randomTopicArrayIndex = Math.floor(Math.random() * topics.length);
//   newTopic.push(topics[randomTopicArrayIndex]);
//   return topics[randomTopicArrayIndex];
// };

// dataMuse api request url for related words
// const baseUrl = `https://api.datamuse.com/words?rel_trg=${}`;
const randomTopic = "guitar";

// dynamically request related words
export const getRelatedWords = async () => {
  const baseUrl = `https://api.datamuse.com/words?rel_trg=${randomTopic}`;
  try {
    const wordArr = [];
    const response = await fetch(baseUrl);
    const data = await response.json();
    for (const result of data) {
      wordArr.push(result.word);
    }
    console.log(wordArr);
    return wordArr;
  } catch (error) {
    console.error(error);
  }
};

export const randomWord = [];

export const pickRandomWord = async () => {
  try {
    const newWordArr = await getRelatedWords();
    // console.log(newWordArr);
    const randomWordArrayIndex = Math.floor(Math.random() * newWordArr.length);
    const searchWord = newWordArr[randomWordArrayIndex];
    randomWord.push(searchWord);
    console.log(searchWord);
    return searchWord;
  } catch (error) {
    console.error(error);
  }
};

function wordReducer(state = {}, action) {
  console.log(action);
  switch (action.type) {
    case "CHANGE_WORD":
      const word = action.word;
      console.log(word);
      return { word };
    default:
      return state;
  }
}

export default wordReducer;
