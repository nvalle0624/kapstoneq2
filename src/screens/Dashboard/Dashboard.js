import Game from "../Game/Dictionary-Game";
import "./dashboard.css";

export const Dashboard = () => {
  return (
    <>
      <div className="dashboard">
        <Game />
      </div>
    </>
  );
};

export default Dashboard;
