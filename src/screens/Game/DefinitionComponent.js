import React from "react";
import { useSelector } from "react-redux";

export default function DefinitionComponent() {
  const definition = useSelector((state) => state.definitionReducer.definition);

  return (
    <>
      <div id="definitionDisplay">Definition:{definition}</div>
    </>
  );
}
