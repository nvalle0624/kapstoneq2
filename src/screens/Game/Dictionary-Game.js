import TopicContainer from "../../redux/containters/TopicContainer";
import WordContainer from "../../redux/containters/WordContainer";
import DefinitionComponent from "./DefinitionComponent";
import PosComponent from "./PosComponent";
import { useDispatch } from "react-redux";

export const Game = (props) => {
  const dispatch = useDispatch();

  return (
    <>
      <div className="gameStage">
        <div className="welcome">Welcome,(user)!!</div>
        <div className="background-dashboard"></div>
        <p className="stats">
          Rank: <br />
          Current Score:
          <br /> Today's Score:
        </p>
      </div>

      <div>
        <button className="button" id="changeTopicButton" onClick={() => dispatch({ type: "CHANGE_TOPIC" })}>
          Spin for Topic
        </button>
      </div>
      <div className="subject">
        <TopicContainer />
      </div>
      <WordContainer />
      <br />
      <DefinitionComponent />
      <br />
      <PosComponent />
    </>
  );
};

export default Game;
