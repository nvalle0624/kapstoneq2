import React from "react";
import { useSelector } from "react-redux";

function PosComponent() {
  const partOfSpeech = useSelector((state) => state.posReducer.pos);

  return (
    <>
      <div id="posDisplay">Part of Speech:{partOfSpeech}</div>
    </>
  );
}

export default PosComponent;
