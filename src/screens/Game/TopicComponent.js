import React from "react";
import { useSelector } from "react-redux";

export default function TopicComponent(props) {
  const topic = useSelector((state) => state.topicReducer.topic);

  return (
    <>
      <div id="subjectDisplay">Subject:{topic}</div>
    </>
  );
}
