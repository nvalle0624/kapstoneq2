import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";

import wise4 from "../pictures/wise4.png";
import rightAnswer from "../Dashboard/audio/correctAnswer.mp3";
import wrongAnswer from "../Dashboard/audio/wrongAnswer.mp3";
function WordComponent(props) {
  const [inputText, setInputText] = useState("");
  const [score, setScore] = useState(0);
  const word = useSelector((state) => state.wordReducer.word);
  const dispatch = useDispatch();

  const randomTopic = props.topicReducer.topic;
  const rightAnswerFx = new Audio(rightAnswer);
  const wrongAnswerFx = new Audio(wrongAnswer);
  // dynamically request related words
  const getRelatedWords = async () => {
    const baseUrl = `https://api.datamuse.com/words?rel_trg=${randomTopic}`;
    try {
      const wordArr = [];
      const response = await fetch(baseUrl);
      const data = await response.json();
      for (const result of data) {
        wordArr.push(result.word);
      }
      console.log(wordArr);
      return wordArr;
    } catch (error) {
      console.error(error);
    }
  };

  const randomWord = [];

  const pickRandomWord = async () => {
    try {
      const newWordArr = await getRelatedWords();
      const randomWordArrayIndex = Math.floor(Math.random() * newWordArr.length);
      const searchWord = newWordArr[randomWordArrayIndex];
      randomWord.push(searchWord);
      console.log(searchWord);
      return searchWord;
    } catch (error) {
      console.error(error);
    }
  };
  const testUrl = (randomWord) =>
    `https://dictionaryapi.com/api/v3/references/collegiate/json/${randomWord}?key=1cdee7be-e272-4024-8826-d6d059d6922c`;

  const definitionArr = [];
  const getDefinitions = async () => {
    try {
      const theWord = randomWord;
      const response = await fetch(testUrl(theWord));
      const data = await response.json();

      const def = await data[0].shortdef.toString();
      await definitionArr.push(def);

      return def;
    } catch (error) {
      console.error(error);
    }
  };

  const getPoS = async () => {
    try {
      const theWord = randomWord;
      const response = await fetch(testUrl(theWord));
      const data = await response.json();
      const pOs = data[0].fl;
      return pOs;
    } catch (error) {
      console.error(error);
    }
  };
  //audio implementated following: https://www.youtube.com/watch?v=3NgVlAscdcA, with help from Gabby and Nico!
  const ctx = new AudioContext();

  async function getAudio() {
    try {
      const response = await fetch(testUrl(word));
      console.log(response);
      const audioData = await response.json();
      const audio = audioData[0].hwi.prs[0].sound.audio;

      const data = await fetch(`https://media.merriam-webster.com/audio/prons/en/us/mp3/${word[0]}/${audio}.mp3`);
      const buffer = await data.arrayBuffer();
      const decodedAudio = await ctx.decodeAudioData(buffer);
      console.log(decodedAudio);
      playback(decodedAudio);
      return response;
    } catch (error) {
      console.error(error);
    }
  }
  async function playback(decodedAudio) {
    console.log(decodedAudio);
    const playSound = await ctx.createBufferSource();
    playSound.buffer = await decodedAudio;
    playSound.connect(ctx.destination);
    playSound.start(ctx.currentTime);
  }
  //

  async function pickaWord() {
    const aWord = await pickRandomWord();
    const theDefinition = await getDefinitions();
    const thePartofSpeech = await getPoS();
    dispatch({ type: "CHANGE_WORD", word: aWord });
    dispatch({ type: "CHANGE_DEFINITION", definition: theDefinition });
    dispatch({ type: "GET_POS", pos: thePartofSpeech });
  }

  const answer = document.getElementById("answer");

  function handleSubmit() {
    if (answer.value === word) {
      rightAnswerFx.play();
      console.log("success");
      setScore(score + 1);
      checkScore();
      pickaWord();
    } else {
      wrongAnswerFx.play();
      console.log("womp womp");
    }
  }

  const winDiv = document.getElementById("showWin");
  function checkScore() {
    if (score === 3) {
      console.log("you win");
      let winImgEl = document.createElement("img");
      winImgEl.src = wise4;
      winDiv.appendChild(winImgEl);
      let winMsg = document.createElement("div");
      winMsg.innerHTML = "YOU WIN!";
      winDiv.appendChild(winMsg);
    }
  }

  const wordDiv = document.getElementById("wordDisplay");
  async function revealWord() {
    wordDiv.innerHTML = await word;
  }

  return (
    <>
      <input
        type="text"
        onChange={(event) => setInputText(event.target.value)}
        value={inputText}
        id="answer"
        className="Gameboard"
        placeholder=""
      />

      <div id="score">{score}</div>
      <div id="wordDisplay">Reveal Word</div>
      <button id="revealButton" onClick={() => revealWord()}>
        Reveal Word
      </button>
      <button className="button" id="changeWordButton" onClick={async () => await pickaWord()}>
        Spin for Word
      </button>

      <button className="button" id="submitButton" onClick={() => handleSubmit()}>
        submit answer
      </button>
      <div id="showWin"></div>

      <button className="button" id="audioButton" onClick={async () => await getAudio()}>
        hear word
      </button>
    </>
  );
}

export default WordComponent;
