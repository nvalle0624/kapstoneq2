// import React, { useState } from "react";
import "../Login/Login.css";
import useAuth from "../../hooks/useAuth";
// import Navigation from "../../components/Navigation";
// import Links from "../../components/Links";

export const LogOut = () => {
  const auth = useAuth();

  if (auth.loading || auth.loggingOut) {
    // User is currently trying to log in or something..
    return "Loading....... 😬";
  }
  return (
    <>
      <div>
        <h1>Log Out</h1>

        {auth.loggedIn ? (
          <div>
            You are logged-in.
            <br />
            <button onClick={() => auth.logout()}>Logout</button>
          </div>
        ) : (
          <div>WompWomp</div>
        )}
      </div>
    </>
  );
};

export default LogOut;
