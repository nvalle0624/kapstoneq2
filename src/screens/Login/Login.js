import React, { useEffect, useState } from "react";
import "./Login.css";
import useAuth from "../../hooks/useAuth";

const Login = () => {
  const [inputText, setInputText] = useState("");
  const [currentUser, setCurrentUser] = useState("");
  const auth = useAuth();
  const emailInput = document.getElementById("emailInput");

  // useEffect(() => {
  //   async function fetchuser() {
  //     if (auth.loggingIn) {
  //       setCurrentUser(await inputText);
  //       console.log(currentUser);
  //     }
  //   }
  //   fetchuser();
  // }, [auth.loggingIn]);

  async function loginNow() {
    try {
      const email = await emailInput;
      await auth.login(email.value);
      getProfile();
      setCurrentUser(email.value);
    } catch (err) {
      console.error(err);
    }
  }

  if (auth.loading || auth.loggingIn || auth.loggingOut) {
    return "Loading.......";
  }

  async function getProfile() {
    await fetch("/api/userProfile", userReqOptions)
      .then((response) => response.json())
      .then((data) => setCurrentUser(data.email.value));
  }
  const userReqOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: {
      email: inputText,
    },
  };

  return (
    <>
      <div className="Login-body">
        <div className="owl" alt="wise owl"></div>

        <form className="form">
          <h3>Sign In</h3>

          <div className="form-group">
            <label>Email address</label>
            <input
              onChange={(event) => setInputText(event.target.value)}
              value={inputText}
              id="emailInput"
              type="email"
              className="form-control"
              placeholder="Enter email"
            />
          </div>

          <div className="form-group">
            <label>Password</label>
            <input id="passwordInput" type="password" className="form-control" placeholder="Enter password" />
          </div>

          <div className="form-group">
            <div className="custom-control custom-checkbox">
              <input type="checkbox" className="custom-control-input" id="customCheck1" />
              <label className="custom-control-label" htmlFor="customCheck1">
                Remember me
              </label>
            </div>
          </div>

          {auth.loggedIn ? (
            <div>
              You are logged-in.
              <br />
              <button onClick={() => auth.logout()}>Logout</button>
            </div>
          ) : (
            <div>
              <button onClick={loginNow}>Login Now</button>
            </div>
          )}
          <p className="forgot-password text-right">
            Forgot <a href="#">password?</a>
          </p>
        </form>
      </div>
      <p className="signinText">If you have an account, go ahead, sign in or press the sign up button.</p>
    </>
  );
};

export default Login;
