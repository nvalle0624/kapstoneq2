import React, { useState } from "react";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
import "./SignUp.css";

const SignUp = () => {
  const [topicSelections, setTopicsSelections] = useState([]);
  const [eduLevelSelections, setEduLevelSelections] = useState([]);
  const [inputText, setInputText] = useState("");
  //   remove from selected topics, needs help, currently resets page
  const handleDeleteTopics = (e) => {
    e.preventDefault();
    for (const topic in topicSelections) {
      if (e.currentTarget.id === topic) {
        topicSelections.splice(topic);
      }
      setTopicsSelections(topicSelections);
    }
  };

  // create a button with the selected topic, which can then be used to remove
  //   the topic from the selected topics after it was added
  const createTopicButton = (topic) => {
    const newButton = document.createElement("button");
    const topicsDiv = document.getElementById("topicsList");
    newButton.className = "topicItem";
    newButton.id = topic;
    newButton.innerHTML = topic;
    newButton.onClick = { handleDeleteTopics };
    topicsDiv.append(newButton);
  };

  // create a button with the selected edu level, which can then be used to remove
  //   the level from the selected levels after it was added
  const createEduButton = (level) => {
    const newButton = document.createElement("button");
    const eduDiv = document.getElementById("eduLevelList");
    newButton.className = "eduItem";
    newButton.id = level;
    newButton.innerHTML = level;
    newButton.onClick = { handleDeleteTopics };
    eduDiv.append(newButton);
  };

  // when a edu level is selected, create a button for removal, and add to/update state
  const handleSelectEdu = (e) => {
    eduLevelSelections.push(e.currentTarget.id);
    createEduButton(e.currentTarget.id);
    setEduLevelSelections(eduLevelSelections);
    console.log(eduLevelSelections);
  };

  // when and interest is selected, create a button for removal, and add to/update state
  const handleSelectInterest = (e) => {
    topicSelections.push(e.currentTarget.id);
    createTopicButton(e.currentTarget.id);
    setTopicsSelections(topicSelections);
    console.log(topicSelections);
  };

  const firstNameTextInput = document.getElementById("firstName");

  const lastNameTextInput = document.getElementById("lastName");

  const emailTextInput = document.getElementById("email");

  const ageInput = document.getElementById("age");

  const passwordTextInput = document.getElementById("password");

  const educationInput = eduLevelSelections;

  const topicsInput = topicSelections;

  const handleSubmit = () => {
    const postReqOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        firstname: firstNameTextInput.value,
        lastname: lastNameTextInput.value,
        email: emailTextInput.value,
        age: ageInput.value,
        active: false,
        password: passwordTextInput.value,
        education: educationInput,
        interests: topicsInput,
      }),
    };
    fetch("/api/game", postReqOptions).then((response) => response.json());
  };

  return (
    <div className="signUp-body">
      <div className="owl2" alt="wise owl"></div>
      <div className="owl3" alt=" owl"></div>
      <div className="wise2" alt="wise owl"></div>
      <p className="greeting">Meet the Team</p>
      <p className="greeting2">and Fellow Graduates</p>
      <p className="name2">Joshua</p>
      <p className="greeting3">We'll be your Tudors</p>
      <p className="name3">Ivan</p>
      <p className="greeting4">I can give a Hint</p>
      <p className="name4">Ma</p>
      <form className="form">
        <h3>Sign Up</h3>

        <div className="form-group">
          <label>First name</label>
          <input
            onChange={(event) => setInputText(event.target.value)}
            value={inputText}
            type="text"
            className="form-control"
            id="firstName"
            placeholder="First name"
          />
        </div>

        <div className="form-group">
          <label>Last name</label>
          <input type="text" className="form-control" id="lastName" placeholder="Last name" />
        </div>

        <div className="form-group">
          <label>Email address</label>
          <input type="email" className="form-control" id="email" placeholder="Enter email" />
        </div>

        <div className="form-group">
          <label>Age</label>
          <input type="number" className="form-control" id="age" placeholder="Age" />
        </div>

        <div className="form-group">
          <label>Password</label>
          <input type="password" className="form-control" id="password" placeholder="Enter password" />
        </div>
        <div className="DropDown">
          <label>Education</label>
          <div id="eduLevelList">
            <DropdownButton alignRight title="Select Education Level" id="dropdown-menu-align-right">
              <Dropdown.Item eventKey="option-1">
                <button id="Have not yet graduated" onClick={handleSelectEdu}>
                  Have not yet graduated
                </button>
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item eventKey="option-2">
                <button id="High School Diploma" onClick={handleSelectEdu}>
                  High School Diploma
                </button>
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item eventKey="option-3">
                <button id="Certified" onClick={handleSelectEdu}>
                  Certified
                </button>
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item eventKey="option-3">
                <button id="AA/AAS" onClick={handleSelectEdu}>
                  AA/AAS
                </button>
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item eventKey="option-3">
                <button id="BA/BS" onClick={handleSelectEdu}>
                  BA/BS
                </button>
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item eventKey="option-3">
                <button id="Masters/Doctorate" onClick={handleSelectEdu}>
                  Masters/Doctorate
                </button>
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item eventKey="option-3">
                <button id="Higher Ed" onClick={handleSelectEdu}>
                  Higher Ed
                </button>
              </Dropdown.Item>
            </DropdownButton>
          </div>
        </div>
        <div className="DropDown">
          <label>Topics of Interest</label>
          <div id="topicsList">
            <DropdownButton alignRight title="Select Topics" id="dropdown-menu-align-right">
              <Dropdown.Item eventKey="option-1">
                <button id="sports" onClick={handleSelectInterest}>
                  Sports
                </button>
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item eventKey="option-2">
                <button id="music" onClick={handleSelectInterest}>
                  Music
                </button>
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item eventKey="option-3">
                <button id="technology" onClick={handleSelectInterest}>
                  Technology
                </button>
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item eventKey="option-3">
                <button id="nature" onClick={handleSelectInterest}>
                  Nature
                </button>
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item eventKey="option-3">
                <button id="animals" onClick={handleSelectInterest}>
                  Animals
                </button>
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item eventKey="option-3">
                <button id="games" onClick={handleSelectInterest}>
                  Games
                </button>
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item eventKey="option-3">
                <button id="art" onClick={handleSelectInterest}>
                  Art
                </button>
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item eventKey="option-3">
                <button id="reading" onClick={handleSelectInterest}>
                  Reading
                </button>
              </Dropdown.Item>
            </DropdownButton>
          </div>
        </div>

        <button onClick={() => handleSubmit()} type="submit" className="btn btn-primary btn-block">
          Sign up
        </button>
        <p className="forgot-password text-right">
          Already registered <a href="/login">sign in?</a>
        </p>
      </form>
    </div>
  );
};

export default SignUp;
